package com.karatel.study.googleapi_work_with_map;

import android.app.Dialog;
import android.content.*;
import android.os.Bundle;
import android.support.v7.app.*;
import android.view.*;
import android.widget.CheckBox;

public class InstructionDialogFragment extends AppCompatDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View v = LayoutInflater.from(getActivity())
                .inflate(R.layout.fragment_instruction_dialog, null);

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                CheckBox checkBox = v.findViewById(R.id.check_box_dialog);
                if (checkBox.isChecked())
                    dontShowInstructionAgain();
            }
        };
        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.instruction_title))
                .setView(v)
                .setPositiveButton(android.R.string.ok, listener)
                .create();

    }

    private void dontShowInstructionAgain(){
        SharedPreferences prefs = getActivity()
                .getSharedPreferences(MapsActivity.SHAREDPREF_SET, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(MapsActivity.SHOW_INSTRUCTION, false);
        editor.apply();
    }
}
