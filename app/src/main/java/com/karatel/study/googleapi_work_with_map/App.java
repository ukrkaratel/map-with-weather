package com.karatel.study.googleapi_work_with_map;

import android.app.Application;

import com.karatel.study.googleapi_work_with_map.data.OpenWeatherAPI;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {
    private static OpenWeatherAPI openWeatherAPI;

    @Override
    public void onCreate() {
        super.onCreate();
        initOpenWeatherAPI();
    }

    private void initOpenWeatherAPI(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        openWeatherAPI = retrofit.create(OpenWeatherAPI.class);
    }

    public static OpenWeatherAPI getAPI(){
        return openWeatherAPI;
    }
}
