package com.karatel.study.googleapi_work_with_map.data;

import retrofit2.Call;
import retrofit2.http.*;

public interface OpenWeatherAPI {
    String appID = "f25afdfcedbd5f3301d0da096d62ef14";
    String unitsFormat = "metric";

    @GET("data/2.5/weather")
    Call<WeatherResponse> getCurrentWeather(@Query("lat") double lat, @Query("lon") double lon,
                                            @Query("units") String tempFormat,
                                            @Query("lang") String lang,
                                            @Query("APPID") String apiKey);

}
