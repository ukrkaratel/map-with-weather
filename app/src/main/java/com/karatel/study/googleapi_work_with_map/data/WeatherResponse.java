package com.karatel.study.googleapi_work_with_map.data;


import com.google.gson.annotations.*;

import java.util.List;

public class WeatherResponse {
    @SerializedName("weather")
    public List<Weather> weather;
    @SerializedName("main")
    public Main main;
    @SerializedName("wind")
    public Wind wind;

    public class Weather{
        @SerializedName("description")
        public String description;
        @SerializedName("icon")
        public String icon;
    }

    public class Main {

        @SerializedName("temp")
        public double temp;
        @SerializedName("pressure")
        public double pressure;
        @SerializedName("humidity")
        public double humidity;
        @SerializedName("temp_min")
        public double tempMin;
        @SerializedName("temp_max")
        public double tempMax;

    }

    public class Wind {

        @SerializedName("speed")
        public double speed;

    }

    public Weather getWeather() {
        return weather.get(0);
    }
}
