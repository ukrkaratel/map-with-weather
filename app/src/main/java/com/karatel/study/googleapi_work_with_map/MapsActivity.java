package com.karatel.study.googleapi_work_with_map;

import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.*;
import com.google.android.gms.location.places.ui.*;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.karatel.study.googleapi_work_with_map.data.*;

import java.util.*;

import retrofit2.*;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    public static final String SHAREDPREF_SET = "just shared prefs";
    public static final String SHOW_INSTRUCTION = "show instruction";
    private static final String LAST_LATITUDE = "last latitude";
    private static final String LAST_LONGITUDE = "last longitude";
    private static final float KYIV_LATITUDE = 50.4501f;
    private static final float KYIV_LONGITUDE = 30.5234f;
    private static final int DEFAULT_ZOOM = 9;

    private SharedPreferences prefs;
    private GoogleMap mMap;
    private HashMap<Marker, WeatherResponse> markersInfo;
    private LatLng currentLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        createMap();
        prefs = getSharedPreferences(SHAREDPREF_SET, MODE_PRIVATE);
        if (savedInstanceState == null && prefs.getBoolean(SHOW_INSTRUCTION, true))
            showInstruction();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveCoords();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // create markersInfo (each marker has its own information about weather)
        markersInfo = new HashMap<>();

        adjustMap();
        // move camera to the last place of the previous session
        getLastCoords();
        moveCamera();

        adjustMapClicks();
        setupSearch();
    }

    private void createMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void showInstruction() {
        new InstructionDialogFragment().show(getSupportFragmentManager(), "instruction");
    }

    private void adjustMap() {
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setMapToolbarEnabled(false);
        uiSettings.setTiltGesturesEnabled(false);
    }

    private void moveCamera() {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, DEFAULT_ZOOM));
    }

    // Kyiv is a default
    private void getLastCoords() {
        float lastLat = prefs.getFloat(LAST_LATITUDE, KYIV_LATITUDE);
        float lastLng = prefs.getFloat(LAST_LONGITUDE, KYIV_LONGITUDE);
        currentLatLng = new LatLng(lastLat, lastLng);
    }

    private void adjustMapClicks() {
        mMap.setInfoWindowAdapter(new WeatherInfoWindowAdapter());
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                currentLatLng = latLng;
                showWeather();
            }
        });
    }

    private void setupSearch() {
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();
        autocompleteFragment.setFilter(typeFilter);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                currentLatLng = place.getLatLng();
                moveCamera();
                showWeather();
            }

            @Override
            public void onError(Status status) {
                Toast.makeText(MapsActivity.this, getString(R.string.search_error),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showWeather() {
        // Get the current language to receive a correct description of the weather
        String lang = Locale.getDefault().getLanguage();
        App.getAPI().getCurrentWeather(currentLatLng.latitude, currentLatLng.longitude,
                OpenWeatherAPI.unitsFormat, lang, OpenWeatherAPI.appID)
                .enqueue(new WeatherCallback());
    }

    private void saveCoords() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(LAST_LATITUDE, (float) currentLatLng.latitude);
        editor.putFloat(LAST_LONGITUDE, (float) currentLatLng.longitude);
        editor.apply();
    }

    private class WeatherCallback implements Callback<WeatherResponse> {

        @Override
        public void onResponse(Call<WeatherResponse> call,
                               Response<WeatherResponse> response) {
            if (response.isSuccessful()) {
                WeatherResponse weatherResponse = response.body();
                createMarker(weatherResponse);
            }
        }

        @Override
        public void onFailure(Call<WeatherResponse> call, Throwable t) {
            t.printStackTrace();
            Toast.makeText(MapsActivity.this, getString(R.string.failure),
                    Toast.LENGTH_LONG).show();
        }

        private void createMarker(WeatherResponse weatherResponse){
            String iconName = "icon_" + weatherResponse.getWeather().icon;
            int iconID = getResources()
                    .getIdentifier(iconName, "drawable",
                            MapsActivity.this.getPackageName());
            Marker marker = mMap.addMarker(new MarkerOptions().position(currentLatLng)
                    .icon(BitmapDescriptorFactory.fromResource(iconID)));
            markersInfo.put(marker, weatherResponse);
        }
    }

    private class WeatherInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        private View view;

        @Override
        public View getInfoWindow(Marker marker) {
            // get the correct weatherResponse
            WeatherResponse weatherResponse = markersInfo.get(marker);
            view = MapsActivity.this.getLayoutInflater()
                    .inflate(R.layout.custom_info_window, null);
            setupText(R.id.description_txt, weatherResponse.getWeather().description);
            setupTemperature(R.id.temperature_txt, weatherResponse.main.temp);
            setupTemperature(R.id.min_temperature_txt, weatherResponse.main.tempMin);
            setupTemperature(R.id.max_temperature_txt, weatherResponse.main.tempMax);
            setupTextWithUnits(R.id.pressure_txt, weatherResponse.main.pressure, R.string.pressure_units);
            setupTextWithUnits(R.id.wind_txt, weatherResponse.wind.speed, R.string.wind_speed_units);
            setupTextWithUnits(R.id.humidity_txt, weatherResponse.main.humidity, R.string.humidity_units);
            return view;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }

        private void setupTemperature(int textViewId, double temp) {
            int temperature = (int) Math.round(temp);
            String tempStr = temperature > 0 ? "+" + temperature : temperature + "";
            setupText(textViewId, tempStr);
        }

        // set text with the units of measure
        private void setupTextWithUnits(int textViewId, double val, int unitsId) {
            setupText(textViewId, val + " " + getString(unitsId));
        }

        private void setupText(int textViewId, String text) {
            ((TextView) view.findViewById(textViewId)).setText(text);
        }
    }
}
